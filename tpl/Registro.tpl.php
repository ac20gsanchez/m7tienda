<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>Usuarios</title>
    <link rel="stylesheet" href="../assets/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="wrapper">
        <?php if (isset($_SESSION["editarCuenta"]) || isset($_SESSION["editarCuentas"])) { // Cambia el texto si se está registrando o editando una cuenta?>
            <h2>Editar cuenta</h2>
        <?php } else { ?>
            <h2>Registrar cuenta</h2>
        <?php } ?>
        <p>Rellena los campos.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; // Comprueba que los valores introducidos son correctos ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>
            <?php if (isset($_SESSION["editarCuentas"]) && $_SESSION["user_rol"] == "admin") { // Sección de editar cuentas si se es admin y se está editando una cuenta ?>
                <div class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Rol</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="adminField" name="makeAdmin" class="form-control" value="admin">
                                <label class="form-check-label" for="adminField">Administrador</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="memberField" name="makeAdmin" class="form-control" value="member">
                                <label class="form-check-label" for="memberField">Miembro</label>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group">
                <input type="submit" name="save" class="btn btn-primary" value="Guardar">
                <input type="reset" class="btn btn-secondary ml-2" value="Reset">
            </div>
            <?php if (isset($_SESSION["editarCuenta"]) || isset($_SESSION["editarCuentas"])) { ?>
                <p><input type="submit" class="btn btn-secondary" name="atras" value="Volver"></p>
            <?php } else { ?>
                <p>Ya tienes una cuenta? <a href="../index.php">Login</a>.</p>
            <?php } ?>
        </form>
    </div>    
</body>
</html>