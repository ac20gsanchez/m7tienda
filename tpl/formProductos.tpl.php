<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>Editar producto</title>
    <link rel="stylesheet" href="../assets/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="wrapper">
        <?php if (isset($_SESSION["insert"])) { ?>
            <h2>Añadir producto</h2>
        <?php } else { ?>
            <h2>Editar producto</h2>
        <?php } ?>
        <p>Introduce los nuevos datos.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Id o lote del producto</label>
                <input type="text" name="id" class="form-control <?php echo (!empty($id_err)) ? 'is-invalid' : ''; // Comprueba que los valores introducidos son correctos ?>" value="<?php echo $id; ?>">
                <span class="invalid-feedback"><?php echo $id_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Nombre del producto</label>
                <input type="text" name="product_name" class="form-control <?php echo (!empty($product_name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $product_name; ?>">
                <span class="invalid-feedback"><?php echo $product_name_err; ?></span>
            </div>
            <div class="form-group">
                <label>Categorías</label>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-10">
                            <?php
                                // Genera los checkbox con las categorías
                                $obj = new metodos();
                                $categorias = $obj->mostrarCategorias(); 
                                foreach ($categorias as $categoria) :  ?>

                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="category<?= $categoria["name"] ?>" name="categorias[]" class="form-control" value="<?= $categoria["id"] ?>">
                                        <label class="form-check-label" for="category<?= $categoria["name"] ?>"><?= $categoria["name"] ?></label>
                                    </div>

                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Precio</label>
                <input type="number" name="price" class="form-control <?php echo (!empty($price_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $price; ?>">
                <span class="invalid-feedback"><?php echo $price_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" name="saveProduct" class="btn btn-primary" value="Guardar">
                <input type="reset" class="btn btn-secondary ml-2" value="Reset">
            </div>
            <p><input type="submit" class="btn btn-secondary" name="atras" value="Volver"></p>
        </form>
    </div>    
</body>
</html>