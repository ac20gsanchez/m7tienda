<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>Tienda</title>
        <link rel="stylesheet" href="../assets/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body>

        <div class="wrapperAplicacion">

        <header>
            <div class="container">
                <div class="row">
                    <?php if ($_SESSION["user_rol"] != "admin") {  // Si el usuario es admin muestra un mensaje personalizado ?>
                        <div class="col">
                            <h4>Hola <strong><?php echo $_SESSION["username"]; ?></strong></h4>
                        </div>
                        <div class="col">
                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <input type="submit" class="btn btn-link" name="editarCuenta" value="Cuenta">
                                <input type="submit" class="btn btn-link" name="verCarrito" value="Ver carrito">
                            </form>
                        </div>
                    <?php } else { ?>
                        <div class="col">
                            <h4>Hola admin <strong><?php echo $_SESSION["username"]; ?></strong></h4>
                        </div>
                        <div class="col">
                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <input type="submit" class="btn btn-link" name="editarCuenta" value="Cuenta">
                                <input type="submit" class="btn btn-link" name="editarCuentas" value="Usuarios">
                                <input type="submit" class="btn btn-link" name="editarProducts" value="Productos">
                                <input type="submit" class="btn btn-link" name="verCarrito" value="Carrito">
                            </form>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </header>

            <hr>

            <form class="productForm" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                <div class="container">
                    <div class="row">

                        <div class="col-sm">
                            <?php
                                // Genera los checkbox con el filtro de categorías
                                $obj = new metodos();
                                $categorias = $obj->mostrarCategorias(); 
                                foreach ($categorias as $categoria) :  ?>

                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="category<?= $categoria["name"] ?>" name="categorias[]" class="form-control" value="<?= $categoria["id"] ?>">
                                        <label class="form-check-label" for="category<?= $categoria["name"] ?>"><?= $categoria["name"] ?></label>
                                    </div>

                            <?php endforeach; ?>
                        </div>

                        <div class="col-sm">
                            <input type="text" class="form-control" name="search" placeholder="<?php if (isset($_SESSION["search"]) && !empty($_SESSION['search'])) { 
                                                                                                            echo $_SESSION["search"]; 
                                                                                                        } else { // Muestra en el placeholder del buscador la última búsqueda realizada
                                                                                                            echo "Buscar producto...";
                                                                                                        } ?>">
                        </div>
                        <div class="col-sm">
                            <input type="submit" class="btn btn-dark ml-2" name="submit" value="Buscar">
                            <input class="btn btn-dark ml-2" name="resetSearch" type="submit" value="Reset">
                        </div>

                    </div>
                </div>

                </br>

                <table class="table table-hover">

                    <thead class="thead-dark">
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </thead>

                    <tbody>

                        <?php
                            // Genera una fila por cada producto
                            $preuTotal = 0;
                            $quantitatTotal = 0;
                            $obj = new metodos();
                            $productos = $obj->mostrarProductos(); 
                            foreach ($productos as $key) : 
                        ?>
                            <tr>
                                <td class="nomPerfum">
                                    <label for="inputPerfum<?= $key["product_id"] ?>"><?= $key["product_name"] ?></label>
                                </td>
                                <td class="lastTD">
                                    <?= $key["product_price"] ?>€
                                </td>
                                <td class="lastTD">    
                                    <input type="number" id="inputPerfum<?= $key["product_id"] ?>" class="products" name="<?= $key["product_id"] ?>" min="0"
                                        value="<?php $quantity=$obj->getCantidad($key["product_id"]); $quantitatTotal += $quantity; echo $quantity; ?>">
                                </td>
                                <td class="lastTD">
                                    <?php $cantidadPrecio = $quantity * $key["product_price"]; $preuTotal += $cantidadPrecio; echo $cantidadPrecio; ?>€
                                </td>
                            </tr>
                        <?php  endforeach; ?>
                    </tbody>

                    <tfoot>

                        <tr>
                            <td colspan="3">
                                Cantidad total de productos
                            </td>
                            <td class="lastTD">
                                <?= $quantitatTotal ?>
                            </td>
                        <tr>
                            <td colspan="3">
                                PRECIO TOTAL
                            </td>
                            <td class="lastTD">
                                <?= $preuTotal; if ($preuTotal == 0) { $_SESSION["emptyCart"] = true; } else { $_SESSION["emptyCart"] = false; }; ?>€
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tableButtons">
                                <input class="btn btn-dark ml-2" type="reset" value="Reset">
                                <input class="btn btn-danger" type="submit" name="buidarCistella" value="Vaciar la cesta">
                                <input class="btn btn-success toTheRight" type="submit" name="actualitzarCistella" value="Guardar cesta">
                            </td>
                        </tr>

                    </tfoot>

                </table>

            </form>
            
            <form method="POST" action="Logout.php">
                <input type="submit" class="btn btn-secondary" name="tancarSessio" value="Salir">
            </form>

        </div>

    </body>
</html>