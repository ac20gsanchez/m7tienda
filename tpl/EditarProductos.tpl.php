<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>Admin Functions</title>
        <link rel="stylesheet" href="../assets/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body>

        <div class="wrapperAplicacion">

            <h2>Editar o añadir productos</h2>

            <form class="productForm" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <input type="text" class="form-control" name="search" placeholder="<?php if (isset($_SESSION["search"]) && !empty($_SESSION['search'])) { 
                                                                                                            echo $_SESSION["search"]; 
                                                                                                        } else { // Muestra en el placeholder del buscador la última búsqueda realizada
                                                                                                            echo "Buscar producto...";
                                                                                                        } ?>">
                        </div>
                        <div class="col-sm">
                            <input type="submit" class="btn btn-dark ml-2" name="submit" value="Buscar">
                            <input class="btn btn-dark ml-2" name="resetSearch" type="submit" value="Reset">
                        </div>
                    </div>
                </div>

                </br>

                <table class="table table-hover">

                    <thead class="thead-dark">
                        <th>Product Id</th>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Opciones</th>
                    </thead>

                    <tbody>

                        <?php
                            // Genera una fila por cada producto
                            $quantitatTotal = 0;
                            $obj = new metodos();
                            $productos = $obj->mostrarProductos(); 
                            foreach ($productos as $key) : 
                        ?>
                            <tr>
                                <td class="lastTD">
                                    <?= $key["product_id"] ?>
                                </td>
                                <td class="nomPerfum">
                                    <?= $key["product_name"] ?>
                                </td>
                                <td class="lastTD">
                                    <?= $key["product_price"] ?>€
                                </td>
                                <td class="lastTD">
                                    <input type="submit" class="btn btn-dark" name="<?= $key["product_id"] ?>" value="Editar">
                                    <input type="submit" class="btn btn-danger" name="<?= $key["product_id"] ?>" value="Eliminar">
                                </td>
                            </tr>
                        <?php
                            $quantitatTotal += 1;
                            endforeach;
                        ?>
                    </tbody>

                    <tfoot>

                        <tr>
                            <td colspan="3">
                                Cantidad total de productos
                            </td>
                            <td class="lastTD">
                                <?= $quantitatTotal ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="lastTD" colspan="4">
                                <input type="submit" class="btn btn-primary" name="insert" value="Añadir producto">
                            </td>
                        </tr>

                    </tfoot>

                </table></br>

                <p><input type="submit" class="btn btn-secondary" name="atras" value="Volver"></p>

            </form>

        </div>

    </body>
</html>