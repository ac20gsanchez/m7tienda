<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Cerrar sesión</title>
    <link rel="stylesheet" href="../assets/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
    <body>

        <div class="wrapperAplicacion">

            <h3>Sesión finalizada</h3>

            <?php if (isset($_SESSION["emptyCart"]) && !$_SESSION["emptyCart"]) { // Muestra la cesta guardada si existe ?>
                <p>Cesta guardada:</p>

                <table class="table table-hover">

                    <thead class="thead-dark">
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </thead>

                    <tbody>
                        <?php 
                            // Genera una fila por cada producto
                            $preuTotal = 0;
                            $quantitatTotal = 0;
                            $obj = new metodos();
                            $productos = $obj->mostrarProductosGuardados(); 
                            foreach ($productos as $key) : 
                        ?>
                                <tr>
                                    <td class="nomPerfum">
                                        <label for="inputPerfum<?= $key["product_id"] ?>"><?= $key["product_name"] ?></label>
                                    </td>
                                    <td class="lastTD">
                                        <?= $key["product_price"] ?>€
                                    </td>
                                    <td class="lastTD">    
                                        <?= $key["product_quantity"]; $quantitatTotal += $key["product_quantity"]; ?>
                                    </td>
                                    <td class="lastTD">
                                        <?php $cantidadPrecio = $key["product_quantity"] * $key["product_price"]; $preuTotal += $cantidadPrecio; echo $cantidadPrecio."€"; ?>
                                    </td>
                                </tr>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <td colspan="3">
                                Cantidad total de productos
                            </td>
                            <td class="lastTD">
                                <?= $quantitatTotal ?>
                            </td>
                        <tr>
                            <td colspan="3">
                                PRECIO TOTAL
                            </td>
                            <td class="lastTD">
                                <?= $preuTotal ?>€
                            </td>
                        </tr>
                    </tfoot>

                </table>

            <?php } ?>

            <p><a href="../index.php" class="btn btn-secondary">Volver al incio</a></p>

        </div>

    </body>
</html>