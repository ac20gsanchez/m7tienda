<?php 

	// Credenciales de la base de datos
    $server = "localhost";
    $user = "root";
    $password = "";
    $dbname = "proyectophp";
    
    // Conexión a la base de datos
    try {
        $db = new PDO("mysql:host=$server;dbname=$dbname", $user, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e){
        // Muestra un error si no ha conseguido conectarse
        echo "Error: " . $e->getMessage();
    }