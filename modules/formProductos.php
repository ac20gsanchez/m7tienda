<?php

    session_start();

    // Comprueba cuando tiempo lleva la sesión inactiva para cerrarla
    require 'CaducitatFunction.php';

    // Conexión a la base de datos
    require_once "conexion.php";

    // Método y funciones de los productos para mostrar la cesta guardada
    require 'Productos.php';

    // Define las variables y las inicializa
    $id = $product_name = $price = "";
    $id_err = $product_name_err = $price_err = "";

    if ( (isset($_SESSION["productoEditar"]) || isset($_SESSION["insert"])) && isset($_SESSION["user_rol"]) && $_SESSION["user_rol"] == "admin" ) {

        // Muestra en los inputs los valores del producto a editar como referencia
        if (!isset($_SESSION["insert"])) {
            $stmt = $db->prepare("SELECT * FROM products WHERE product_id = :product_id");
            $stmt->bindParam(":product_id", $_SESSION["productoEditar"]);
            $stmt->execute();
            $product = $stmt->fetchAll();
            
            $id = $product[0]["product_id"];
            $product_name = $product[0]["product_name"];
            $price = $product[0]["product_price"];
        }
            
        if(isset($_POST["saveProduct"])){

            // Valida que el id del producto contenga caracteres correctos
            if(empty(trim($_POST["id"]))){
                $id_err = "Introduce un id de producto correcto.";
            } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["id"]))){
                $id_err = "El id de producto solo puede contener carácteres normales y números.";
            } elseif(strlen(trim($_POST["id"])) > 20){
                $id_err = "El id de producto no puede contener más de 20 carácteres.";
            } elseif (isset($_SESSION["insert"])) {

                // Busca el id de del producto
                $stmt = $db->prepare("SELECT product_id FROM products WHERE product_id = :product_id");
                $stmt->bindParam(":product_id", $param_product_id);
                $param_product_id = trim($_POST["id"]);
                
                // Si el id ya existe muestra un aviso
                if($stmt->execute()){
                    if($stmt->rowCount() == 1){
                        $id_err = "Este id de producto ya existe.";
                        $check = false;
                    } else{
                        $id = trim($_POST["id"]);
                    }
                } else{
                    echo "Oops! Ha ocurrido un error inesperado. Prueba a intentarlo de nuevo.";
                }
            } else {
                    $id = trim($_POST["id"]);
            }
            // Valida el nombre del producto
            if(empty(trim($_POST["product_name"]))){
                $product_name_err = "Introduce un nombre de producto correcto.";
            } elseif (isset($_SESSION["insert"])) {

                // Busca el producto
                $stmt = $db->prepare("SELECT product_name FROM products WHERE product_name = :product_name");
                $stmt->bindParam(":product_name", $param_product_name);
                $param_product_name = $_POST["product_name"];
                
                // Si el producto ya existe muestra un aviso
                if($stmt->execute()){
                    if($stmt->rowCount() == 1){
                        $product_name_err = "Este producto ya existe.";
                        $check = false;
                    } else{
                        $product_name = $_POST["product_name"];
                    }
                } else{
                    echo "Oops! Ha ocurrido un error inesperado. Prueba a intentarlo de nuevo.";
                }

            } else {
                    $product_name = $_POST["product_name"];
            }
            // Valida el precio del producto
            if(empty(trim($_POST["price"]))){
                $price_err = "Introduce un precio correcto.";
            } elseif(!preg_match('/^[1-9][0-9]{0,15}$/', trim($_POST["price"]))){
                $price_err = "El precio ha de ser mayor que 0, o inferior a la cifra introducida.";
            } else{
                $price = trim($_POST["price"]);
            }

            // Check input errors antes de introducir los datos en la base de datos
            if (empty($id_err) && empty($product_name_err) && empty($price_err)){
                if (isset($_SESSION["insert"])) { // Prepara la consulta para insertar si se está insertando un producto nuevo
                    $stmt = $db->prepare("INSERT INTO products (product_id, product_name, product_price) VALUES (:product_id, :product_name, :product_price)");
                } else { 
                    // Prepara la consulta para modificar el producto
                    $stmt = $db->prepare("UPDATE products SET product_id = :product_id, product_name = :product_name, product_price = :product_price WHERE product_id = :old_product_id");
                    $stmt->bindParam(":old_product_id", $_SESSION["productoEditar"]);
                }
                
                // Variables de la consulta
                $stmt->bindParam(":product_id", $id);
                $stmt->bindParam(":product_name", $product_name);
                $stmt->bindParam(":product_price", $price);
                $stmt->execute();

                // Elimina todas las categorías del producto al editarlo para insertar las nuevas seleccionadas
                $stmt1 = $db->prepare("DELETE FROM product_categories WHERE product_id = :product_id");
                $stmt1->bindParam(":product_id", $id);
                $stmt1->execute();

                // Inserta las categorias seleccionadas del producto
                if (!empty($_POST['categorias'])) {
                    foreach ($_POST['categorias'] as $categoria) {
                        $stmt2 = $db->prepare("INSERT INTO product_categories (product_id, category_id) VALUES (:product_id, :category_id) ON DUPLICATE KEY UPDATE product_id = :product_id, category_id = :category_id");
                        $stmt2->bindParam(":product_id", $id);
                        $stmt2->bindParam(":category_id", $categoria);
                        $stmt2->execute();
                    }
                }
                
                $db = null;

                // Elimina las sesiones para evitar errores en caso de que se salte de una página a otra
                if (isset($_SESSION["insert"])) {
                    unset($_SESSION['insert']);
                }
                if (isset($_SESSION["productoEditar"])) {
                    unset($_SESSION['productoEditar']);
                }
                // Redirige a la ventana del admin
                header("location:AdminFunctions.php");
                exit;
            } 
            
        // Si se pulsa el botón de volver se elimnan las sesiones para evitar errores en caso de que se salte de una página a otra
        } elseif (isset($_POST["atras"])) {
            if (isset($_SESSION["insert"])) {
                unset($_SESSION['insert']);
            }
            if (isset($_SESSION["productoEditar"])) {
                unset($_SESSION['productoEditar']);
            }
            // Redirige a la ventana del admin
            header("location:AdminFunctions.php");
            exit;
        }
        // Incluye el html
        require '../tpl/formProductos.tpl.php';
    } else { // Destruye la sesión y lleva al inicio
        session_destroy();
        header("location:../index.php");
        exit;
    }