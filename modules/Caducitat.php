<?php

    // Inicia la sesión  para comprobar las variables de sesión
    session_start();

    // Método y funciones de los productos para mostrar la cesta guardada
    require 'Productos.php';
    
    // Destruye la sesión
    session_destroy();

    // Incluye el html
    require '../tpl/Caducitat.tpl.php';