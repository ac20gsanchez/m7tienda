<?php

    session_start();

    // Comprueba cuanto tiempo lleva la sesión inactiva para cerrarla
    require 'CaducitatFunction.php';

    // Elimina sesiones que puedan quedar activas en caso que se acceda des de otras páginas para evitar errores
    if (isset($_SESSION["editarCuenta"])) {
        unset($_SESSION['editarCuenta']);
    }
    if (isset($_SESSION["editarCuentas"])) {
        unset($_SESSION['editarCuentas']);
    }
    if (isset($_SESSION["editarProducts"])) {
        unset($_SESSION['editarProducts']);
    }
    if (isset($_SESSION["insert"])) {
        unset($_SESSION['insert']);
    }

    // Comprueba que el usuario ha iniciado sesión si no vuelve al login
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        session_destroy();
        header("location: ../index.php");
        exit;
    }

    // Método y funciones de los productos
    require 'Productos.php';

    // Según el input se mostrará la página de editar usuario, cuentas, o productos
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        // Editar cuenta
        if (isset($_POST["editarCuenta"])) {
            $_SESSION["editarCuenta"] = true;
            header("location: Registro.php");
            exit;
        }

        // Editar cuentas (admins)
        if (isset($_POST["editarCuentas"])) {
            $_SESSION["editarCuentas"] = true;
            header("location: AdminFunctions.php");
            exit;
        }

        // Editar o añadir productos (admins)
        if (isset($_POST["editarProducts"])) {
            $_SESSION["editarProducts"] = true;
            header("location: AdminFunctions.php");
            exit;
        }

        // Ver carrito
        if (isset($_POST["verCarrito"])) {
            header("location: Carrito.php");
            exit;
        }

        // Si se modifican los inputs del carrito se actualiza
        if (isset($_POST["actualitzarCistella"]) || isset($_POST["buidarCistella"])) {
            $obj = new metodos();
            $productCartGuardar = $obj->mostrarProductos();
            $obj2 = new metodos();
            $obj2->guardarCarrito($productCartGuardar); 
        }
        
    }

    // Incluye el html
    require '../tpl/Aplicacio.tpl.php';