<?php   

    define("TEMPSINACTIU", 3600); // Segundos que puede estar la sesión inactiva

    // Tiempo des del último acceso a la página
    $tempsTranscorregut = time() - $_SESSION["ultimAcces"];

    if ($tempsTranscorregut >= TEMPSINACTIU) { // Si la sesión ha caducado
        session_destroy();
        header("Location: Caducitat.php"); // Pagina de caducitat
        exit;
    } else { // Si no ha caducado...
        $_SESSION["ultimAcces"] = time(); // Actualizamos el tiempo activo
    }