<?php

	// Inicia la sesión  para comprobar las variables de sesión
	session_start();
	
	// Si el usuario ya ha inciado sesión se le lleva a la aplicacion
	if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
		header("location: modules/Aplicacio.php");
		exit;
	} else {
		session_destroy();
	}
	
	// Conexión a la base de datos
	require_once "conexion.php";
	
	// Define las variables y las inicializa
	$username = $password = "";
	$username_err = $password_err = $login_err = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST"){
	
		// Comprueba que el username no esté vacío
		if(empty(trim($_POST["username"]))){
			$username_err = "Introduce un nombre de usuario.";
		} else{
			$username = trim($_POST["username"]);
		}
		
		// Comprueba que el password no esté vacío
		if(empty(trim($_POST["password"]))){
			$password_err = "Introduce una contraseña.";
		} else{
			$password = trim($_POST["password"]);
		}
		
		// Check input errors antes de introducir los datos en la base de datos
		if(empty($username_err) && empty($password_err)){
			// Consulta sql
			$sql = "SELECT id, username, password FROM users WHERE username = :username";
			if($stmt = $db->prepare($sql)){
				$stmt->bindParam(":username", $param_username);
				$param_username = $username;
				
				// Ejecuta la consulta
				if($stmt->execute()){
					// Guarda el resultado en cada variable
					while($row=$stmt->fetch()){
						$id=$row['id'];
						$username=$row['username'];
						$hashed_password=$row['password'];
				   	}
					// Comprueba que el nombre de usuario exista
					if($stmt->rowCount() == 1){                    
						if(password_verify($password, $hashed_password)){
							// Comprueba que la contraseña coincida
							session_start();
							
							// Guarda variables de sesión útiles
							$_SESSION["loggedin"] = true;
							$_SESSION["id"] = $id;
							$_SESSION["username"] = $username;
							$_SESSION["ultimAcces"] = time();

							// Guarda el rol del usuario en sesión
							$stmt = $db->prepare("SELECT rol FROM user_roles WHERE username = :username");
							$stmt->bindParam(":username", $username);
							$stmt->execute();
							$_SESSION["user_rol"] = $stmt->fetchColumn();

							// Redirect a la aplicación
							header("location: modules/Aplicacio.php");
							exit;
						} else{
							// Si la contraseña no es correcta muestra un mensaje de error
							$login_err = "Usuario o contraseña incorrectos.";
						}
					} else{
						// Si el usuario no es correcto muestra un mensaje de error
						$login_err = "Usuario o contraseña incorrectos.";
					}
				} else{ // Si hay algún error con la consulta o la base de datos muestra un error
					echo "Oops! Ha ocurrido un error inesperado. Prueba de otra vez.";
				}

			}
		}
		
		// Close connection
		$db = null;
	}