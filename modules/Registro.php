<?php

    session_start();

    // Si no hay una sesión iniciada se destruye
    if (!isset($_SESSION["username"])) {
        session_destroy();
    } else {
        // Comprueba cuando tiempo lleva la sesión inactiva para cerrarla
        require 'CaducitatFunction.php';
    }

    // Include config file
    require_once "conexion.php";
    
    // Define variables and initialize with empty values
    $username = $password = $confirm_password = "";
    $username_err = $password_err = $confirm_password_err = "";

    // Muestra en los inputs los valores del usuario a editar como referencia
    if(isset($_SESSION["editarCuenta"]) || isset($_SESSION["editarCuentas"])) { 
        $stmt = $db->prepare("SELECT * FROM users WHERE id = :old_username_id");
        if (isset($_SESSION["editarCuentas"])) {
            $stmt->bindParam(":old_username_id", $_SESSION["editarCuentas"]);
        } else {
            $stmt->bindParam(":old_username_id", $_SESSION["id"]);
        }
        $stmt->execute();
        $user = $stmt->fetchAll();
        
        $username = $user[0]["username"];
    }
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Botón atrás
        if (isset($_POST["atras"])) {
            header("location:Aplicacio.php");
            exit;
        }

        if (isset($_POST["save"])) {

            // Validate username
            if(empty(trim($_POST["username"]))){ // Comprueba si el campo está vacío
                $username_err = "Introduce un nombre de usuario.";
            } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"]))){ // Comprueba que no contenga caracteres no válidos
                $username_err = "El nombre de usuario solo puede contener letras, números y barra bajas.";
            } elseif(!isset($_SESSION["editarCuenta"]) && !isset($_SESSION["editarCuentas"])) { 
                // Si es un registro de un usuario nuevo se comprueba que no exista ya en la base de datos
                $stmt = $db->prepare("SELECT * FROM users WHERE username = :username"); 
                $param_username = trim($_POST["username"]);
                $stmt->bindParam(":username", $param_username);
                
                // Ejecuta la consulta
                if($stmt->execute()){
                    if($stmt->rowCount() == 1){ // Si la consulta devuelve 1 fila es que el nombre de usuario ya existe
                        $username_err = "Este usuario ya existe.";
                    } else{ // Guarda el nombre de usuario introducido
                        $username = trim($_POST["username"]);
                    }
                } else{ // Muestra un error si falla la consulta o hay qualquier otro problema con la base de datos
                    echo "Oops! Ha ocurrido un error inesperado. Prueba de nuevo.";
                }

            } else {
                $username = trim($_POST["username"]);
            }
            
            // Validate password
            if(empty(trim($_POST["password"]))){ // Comprueba que el campo no esté vacío
                $password_err = "Introduce una contraseña.";     
            } elseif(strlen(trim($_POST["password"])) < 3){ // Comprueba que la contraseña contenga al menos 3 caracteres
                $password_err = "La contraseña tiene que tener al menos 3 caracteres.";
            } else { // Guarda el password introducido
                $password = trim($_POST["password"]);
            }
            
            // Validate confirm password
            if(empty(trim($_POST["confirm_password"]))){ // Comprueba que el campo no esté vacío
                $confirm_password_err = "Confirma la contraseña.";     
            } else { // Comprueba que las contraseñas coincidan
                $confirm_password = trim($_POST["confirm_password"]);
                if(empty($password_err) && ($password != $confirm_password)){
                    $confirm_password_err = "Las contraseñas no coinciden";
                }
            }
        
            // Check input errors antes de introducir los datos en la base de datos
            if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
                // Preparamos la sentencia según si es para introducir un nuevo usuario o para editarlo
                if (isset($_SESSION["editarCuenta"]) || isset($_SESSION["editarCuentas"])) {
                    $stmt = $db->prepare("UPDATE users SET username = :username, password = :user_password WHERE id = :old_username_id");
                    if (isset($_SESSION["editarCuentas"])) {
                        $stmt->bindParam(":old_username_id", $_SESSION["editarCuentas"]);
                    } else {
                        $stmt->bindParam(":old_username_id", $_SESSION["id"]);
                    }
                } else { // Si se introduce un nuevo usuario en lugar de editar se inserta un nuevo usuario en la base de datos
                    $stmt = $db->prepare("INSERT INTO users (username, password) VALUES (:username, :user_password)");
                }

                // Set parameters
                $param_password = password_hash($password, PASSWORD_DEFAULT); // Crea un hash del password para no poder leer las contraseñas de los usuarios

                // Enlaza las variables que se usuarán a la consulta
                $stmt->bindParam(":username", $username);
                $stmt->bindParam(":user_password", $param_password);
                $stmt->execute();
                
                // User rol
                if (!isset($_SESSION["editarCuenta"]) && !isset($_SESSION["editarCuentas"]) && !isset($_SESSION["user_rol"]) && $_SESSION["user_rol"] != "admin") {
                    $rol = "member"; // Si no se especifica ningún rol se establece el rol member por defecto
                    $stmt = $db->prepare("INSERT INTO user_roles (username, rol) VALUES (:username, :rol)");
                    $stmt->bindParam(":username", $username);
                    $stmt->bindParam(":rol", $rol);
                    $stmt->execute();
                } elseif (!isset($_POST["makeAdmin"]) && !isset($_SESSION["editarCuentas"]) && isset($_SESSION["user_rol"]) && $_SESSION["user_rol"] == "admin") {
                    $rol = "admin"; // Si eres admin y no seleccionas un rol se deja admin por defecto
                    $stmt = $db->prepare("UPDATE user_roles SET rol = :rol WHERE username = :username");
                    $stmt->bindParam(":username", $username);
                    $stmt->bindParam(":rol", $rol);
                    $stmt->execute();
                } elseif (isset($_POST["makeAdmin"])) {
                    $rol = trim($_POST["makeAdmin"]); // Cambia el rol del usuario por el seleccionado
                    $stmt = $db->prepare("UPDATE user_roles SET rol = :rol WHERE username = :username");
                    $stmt->bindParam(":username", $username);
                    $stmt->bindParam(":rol", $rol);
                    $stmt->execute();
                }
                
                // Redirige según la página que estés a otra una vez terminado de editaro o añadir
                if (isset($_SESSION["editarCuenta"])) {
                    $_SESSION["username"] = $username; // Cambia el usuario antigio en sesión por el nombre de usuario actualizado
                    unset($_SESSION['editarCuenta']);
                    header("location:Aplicacio.php");
                    exit;
                } elseif (isset($_SESSION["editarCuentas"])) {
                    header("location:AdminFunctions.php");
                    exit;
                } else {
                    header("location: ../index.php");
                    exit;
                }

            }
        }

        // Cierra la conexión
        $db = null;
    }

    // Incluye el html
    require '../tpl/Registro.tpl.php';