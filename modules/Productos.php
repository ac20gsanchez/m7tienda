<?php

    /**
     * Funciones para los productos
     */
    class metodos{
        /**
         * Retorna todos los productos
         * @return Array
         */
        public function mostrarProductos(){
            // Devuelve un array con todos los proudctos y sus datos
            require 'conexion.php';

            $stmt = $db->prepare("SELECT product_id, product_name, product_price FROM products");

            // Filtro de búsqueda de productos
            if ( isset($_POST["resetSearch"])  ) {
                if (isset($_SESSION["search"])) {
                    unset($_SESSION["search"]); // Resetea la búsqueda
                }
            }
            
            if (isset($_SESSION["search"])) { // Si ya se ha realizado una búsqueda se mantiene
                $search = "%".trim($_SESSION["search"])."%";
                $stmt = $db->prepare("SELECT product_id, product_name, product_price FROM products WHERE UPPER(product_name) LIKE UPPER(:search)");
                $stmt->bindParam(':search', $search);
            } 

            if(isset($_POST["search"])){ // Filtra los propductos por la palabra introducida
                $search = "%".trim($_POST["search"])."%";
                $_SESSION["search"] = trim($_POST["search"]);
                $stmt = $db->prepare("SELECT product_id, product_name, product_price FROM products WHERE UPPER(product_name) LIKE UPPER(:search)");
                $stmt->bindParam(':search', $search);

                // Si filtra por categoría pero no por búsqueda
                if (!empty($_POST['categorias']) && empty($_POST['search'])) {
                    foreach ($_POST['categorias'] as $categoria) {
                        // Filtra por categoría
                        $stmt = $db->prepare("SELECT products.product_id, products.product_name, products.product_price 
                                                FROM product_categories
                                                INNER JOIN products ON product_categories.product_id = products.product_id
                                                INNER JOIN categories ON product_categories.category_id = categories.id
                                                WHERE categories.id = :category_id");

                        $stmt->bindParam(":category_id", $categoria);
                        $productos = $stmt->fetchAll();
                        $stmt->execute();
                    }
                }

                // Si filtra por categoría y búsqueda
                if (!empty($_POST['categorias']) && !empty($_POST['search'])) {
                    foreach ($_POST['categorias'] as $categoria) {
                        // Filtra por categoría y nombre
                        $stmt = $db->prepare("SELECT products.product_id, products.product_name, products.product_price 
                                                FROM product_categories
                                                INNER JOIN products ON product_categories.product_id = products.product_id
                                                INNER JOIN categories ON product_categories.category_id = categories.id
                                                WHERE categories.id = :category_id AND UPPER(products.product_name) LIKE UPPER(:search)");

                        $stmt->bindParam(":category_id", $categoria);
                        $stmt->bindParam(':search', $search);
                        $productos = $stmt->fetchAll();
                        $stmt->execute();
                    }
                }
            }
            
            // Ejecuta la consulta
            $stmt->execute();
            $productos = $stmt->fetchAll();
            $db = null;
            
            return $productos;
        }
        
        /**
         * Guarda productos en el carrito
         * @param Array Productos para guardar en el carrito
         */
        public function guardarCarrito($productCart){
            // Guarda y actualiza el carrito
            require 'conexion.php';

            $userName = $_SESSION["username"];

            // Crea el carrito si no existe
            $stmt = $db->prepare("INSERT INTO cart (username) VALUES (:username) ON DUPLICATE KEY UPDATE username = :username");
            $stmt->bindParam(':username', $userName);
            $stmt->execute();

            // Elimina productos del carrito
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                // Vaciar carrito
                if (isset($_POST["buidarCistella"])) {
                    // Elimina el carrito
                    $stmt = $db->prepare("DELETE FROM cart WHERE username = :username");
                    $stmt->bindParam(':username', $userName);
                    $stmt->execute();
                    if (isset($_SESSION["search"])) {
                        unset($_SESSION["search"]);
                    }
                    header("location:Aplicacio.php");
                    exit;
                }
                
            }

            // Consulta para insertar el producto en el carrito, si ya existe lo actualiza
            $stmt = $db->prepare("INSERT INTO cart_products (username, product_id, product_quantity) VALUES (:username, :productId, :productQuantity) ON DUPLICATE KEY UPDATE username = :username, product_id = :productId, product_quantity = :productQuantity");
            $stmt->bindParam(':username', $userName);
            $stmt->bindParam(':productId', $productId);
            $stmt->bindParam(':productQuantity', $productQuantity);

            // Ejecuta el INSERT por cada producto
            foreach ($productCart as $producto) {
                if (isset($_POST[$producto["product_id"]])) {
                    $productId = $producto["product_id"];
                    $productQuantity = trim($_POST[$producto["product_id"]]);
                    $stmt->execute();
                }
            }

            // Elimina todos los productos guardados a 0
            $stmt = $db->prepare("DELETE FROM cart_products WHERE username = :username AND product_quantity = 0");
            $stmt->bindParam(':username', $userName);
            $stmt->execute();
            
            // Cerrar conexiones
            $db = null;
        }

        /**
         * Retorna las cantidades de un producto
         * @param string $product ID del producto
         * @return int
         */
        public function getCantidad($product){
            // Devuelve la cantidad de productos guardados en el carrito de un usuario de un producto recibido
            require 'conexion.php';

            $stmt = $db->prepare("SELECT product_quantity FROM cart_products WHERE product_id = :productId AND username = :username");
            $stmt->bindParam(':productId', $product);
            $stmt->bindParam(':username', $_SESSION["username"]);
            $stmt->execute();
            if($stmt->rowCount() == 1){ // Si existe 1 fila en la consulta significa que existe en el carrito, si no la cantidad será 0
                $cantidad = $stmt->fetchColumn();
            } else {
                $cantidad = 0;
            }

            // Cerrar conexiones
            $db = null;

            return $cantidad;
        }

        /**
         * Return de los productos guardados en el carrito
         */
        public function mostrarProductosGuardados(){
            // Devuelve un array con todos los proudctos guardados en el carrito y sus datos
            require 'conexion.php';

            $userName = $_SESSION["username"];

            // Busca los productos guardados en el carrito del usuario
            $stmt = $db->prepare("SELECT products.product_id, products.product_name, products.product_price, cart_products.product_quantity 
                                    FROM cart_products 
                                    INNER JOIN products ON cart_products.product_id = products.product_id
                                    WHERE cart_products.username = :username");

            $stmt->bindParam(':username', $userName);
            
            // Ejecuta la consulta
            $stmt->execute();
            $productos = $stmt->fetchAll();
            $db = null;
            return $productos;
        }


        /**
         * Return de todas las categorías
         */
        public function mostrarCategorias(){
            // Devuelve un array con todas las categorías
            require 'conexion.php';

            $stmt = $db->prepare("SELECT id, name FROM categories");
            
            // Ejecuta la consulta
            $stmt->execute();
            $categorias = $stmt->fetchAll();
            $db = null;
            return $categorias;
        }

    }