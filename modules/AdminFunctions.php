<?php

    session_start();

    // Comprueba cuando tiempo lleva la sesión inactiva para cerrarla
    require 'CaducitatFunction.php';

    /**
     * Funciones para modificar usuarios
     */
    class metodosUsuario{

        /**
         * Devuelve un array con todos los usuarios registrados
         * @return Array
         */
        public function mostrarUsuarios(){
            require 'conexion.php';

            $stmt = $db->prepare("SELECT * FROM users");

            // Filtro de búsqueda de usuarios
            if ( isset($_POST["resetSearch"])  ) {
                if (isset($_SESSION["search"])) {
                    unset($_SESSION["search"]); // Resetea la búsqueda
                }
            }

            if (isset($_SESSION["search"])) { // Si ya se ha realizado una búsqueda se mantiene
                $search = "%".trim($_SESSION["search"])."%";
                $stmt = $db->prepare("SELECT * FROM users WHERE UPPER(username) LIKE UPPER(:search)");
                $stmt->bindParam(':search', $search);
            } 

            if(isset($_POST["search"])){ // Filtra los usuarios por la palabra introducida
                $search = "%".trim($_POST["search"])."%";
                $_SESSION["search"] = trim($_POST["search"]);
                $stmt = $db->prepare("SELECT * FROM users WHERE UPPER(username) LIKE UPPER(:search)");
                $stmt->bindParam(':search', $search);
            }

            // Ejecuta la consulta
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            return $users;
        }
        
    }

    // Elimina las sesiones para evitar errores en caso de que se salte de una página a otra
    if (isset($_SESSION["insert"])) {
        unset($_SESSION['insert']);
    }
    if (isset($_SESSION["productoEditar"])) {
        unset($_SESSION['productoEditar']);
    }

    // Si se accede des de un usuario que no sea administrador te redirige al inicio
    if (!isset($_SESSION["user_rol"]) && $_SESSION["user_rol"] != "admin") {
        session_destroy();
        header("location:../index.php");
        exit;
    }

    // Include conection file
    require_once "conexion.php";

    // Método y funciones de los productos
    require 'Productos.php';

    // Si se accede a editar cuentas de usuario
    if (isset($_SESSION["editarCuentas"])) {
        if($_SERVER["REQUEST_METHOD"] == "POST"){

            if (isset($_POST["atras"])) { // Si se pulsa el botón de volver se redirige a la aplicación
                header("location:Aplicacio.php");
                exit;
            }

            // Comprueba el username guardado en el post para eliminarlo o editarlo
            $obj = new metodosUsuario();
            $users = $obj->mostrarUsuarios(); 
            foreach ($users as $key) :
                if (isset($_POST[$key["username"]]) && $_POST[$key["username"]] == "Eliminar") {
                    // Si se pulsa eliminar se ejecuta un delete para eliminar el usuario de la base de datos
                    $stmt = $db->prepare("DELETE FROM users WHERE username = :username");
                    $stmt->bindParam(":username", $key["username"]);
                    $stmt->execute();
                } else if (isset($_POST[$key["username"]]) && $_POST[$key["username"]] == "Editar") {
                    // Mediante una variable de sesión se utiliza el mismo código para registrar un usuario para editarlo
                    $_SESSION["editarCuentas"] = $key["id"];
                    header("location:Registro.php");
                    exit;
                }
            endforeach;

            // Cierra conexiones
            $db = null;
        }
        // Inlcuye el html
        require '../tpl/EditarCuentas.tpl.php';

    } else if (isset($_SESSION["editarProducts"])) {
        // Si se accede a editar productos
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // Comprueba el producto guardado en el post para eliminarlo o editarlo
            $obj = new metodos();
            $productos = $obj->mostrarProductos(); 
            foreach ($productos as $key) :
                if (isset($_POST[$key["product_id"]]) && $_POST[$key["product_id"]] == "Eliminar") {
                    // Si se pulsa eliminar se ejecuta un delete para eliminar el producto de la base de datos
                    $stmt = $db->prepare("DELETE FROM products WHERE product_id = :product_id");
                    $stmt->bindParam(":product_id", $key["product_id"]);
                    $stmt->execute();
                } else if (isset($_POST[$key["product_id"]]) && $_POST[$key["product_id"]] == "Editar") {
                    // Redirige a la página para editar y añadir productos guardando el producto a editar en una sesión
                    $_SESSION["productoEditar"] = $key["product_id"];
                    header("location:formProductos.php");
                    exit;
                }
            endforeach;

            // Cierra conexiones
            $db = null;

            // Si se accede a insertar lleva al formulario de añadir y editar productos indicando con una sesión que se va a insertar uno nuevo
            if (isset($_POST["insert"])) {
                $_SESSION["insert"] = true;
                header("location:formProductos.php");
                exit;
            }

            // Si se pulsa el botón de volver se elimnian sesiones y se vuleve a la aplicación
            if (isset($_POST["atras"])) {
                if (isset($_SESSION["editarCuentas"])) {
                    unset($_SESSION['editarCuentas']);
                }
                if (isset($_SESSION["editarProducts"])) {
                    unset($_SESSION['editarProducts']);
                }
                header("location:Aplicacio.php");
                exit;
            }  

            // Redirige a la página del administrador
            header("location:AdminFunctions.php");
            exit;
        }
        
        // Incluye el html
        require '../tpl/EditarProductos.tpl.php';

    } else {
        if (isset($_POST["atras"])) { // Si se pulsa el botón de volver se elimnian sesiones y se vuleve a la aplicación 
            if (isset($_SESSION["editarCuentas"])) {
                unset($_SESSION['editarCuentas']);
            }
            if (isset($_SESSION["editarProducts"])) {
                unset($_SESSION['editarProducts']);
            }
            header("location:Aplicacio.php");
            exit;
        }
        // Se destruye la sesión y vuelve al incio
        session_destroy();
        header("location:../index.php");
        exit;
    }