<?php

    // Inicia la sesión  para comprobar las variables de sesión
    session_start();

    // Comprueba cuando tiempo lleva la sesión inactiva para cerrarla
    require 'CaducitatFunction.php';

    // Elimina sesiones que puedan quedar activas en caso que se acceda des de otras páginas para evitar errores
    if (isset($_SESSION["editarCuenta"])) {
        unset($_SESSION['editarCuenta']);
    }
    if (isset($_SESSION["editarCuentas"])) {
        unset($_SESSION['editarCuentas']);
    }
    if (isset($_SESSION["editarProducts"])) {
        unset($_SESSION['editarProducts']);
    }
    if (isset($_SESSION["insert"])) {
        unset($_SESSION['insert']);
    }

    // Comprueba que el usuario ha iniciado sesión si no vuelve al login
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        session_destroy();
        header("location: ../index.php");
        exit;
    }

    // Elimina las sesiones para evitar errores en caso de que se salte de una página a otra
    if (isset($_SESSION["insert"])) {
        unset($_SESSION['insert']);
    }
    if (isset($_SESSION["productoEditar"])) {
        unset($_SESSION['productoEditar']);
    }

    // Si se accede des de un usuario que no sea administrador te redirige al inicio
    if (!isset($_SESSION["user_rol"]) && $_SESSION["user_rol"] != "admin") {
        session_destroy();
        header("location:../index.php");
        exit;
    }

    // Método y funciones de los productos para mostrar la cesta guardada
    require 'Productos.php';

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        // Si se pulsa el botón atrás
        if (isset($_POST["atras"])) {
            header("location: Aplicacio.php");
            exit;
        }
        
    }

    // Incluye el html
    require '../tpl/Carrito.tpl.php';